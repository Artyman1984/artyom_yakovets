<?php

namespace console\components;

use Closure;

/**
 * Class MultiCURL
 * @package app\console\components
 */
class MultiCURL
{
    /**
     * @var array
     */
    private static $defaultOptions = [
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPGET => true,
        CURLOPT_FOLLOWLOCATION => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_HEADER => false,
        CURLOPT_CONNECTTIMEOUT => 45,
        CURLOPT_TIMEOUT => 45,
    ];

    /**
     * @var array
     */
    private $requests = [];

    /**
     * @param string $url
     * @param Closure $callback
     * @param array $options
     */
    public function add(string $url, Closure $callback, array $options = []): void
    {
        // Create resource.
        $ch = curl_init();
        $options = $options + self::$defaultOptions;
        $options[CURLOPT_URL] = $url;

        curl_setopt_array($ch, $options);

        $this->requests[$this->getResourceId($ch)] = ['handle' => $ch, 'callback' => $callback];
    }

    /**
     * @param resource $resource
     * @return int
     */
    private function getResourceId($resource): int
    {
        if (!is_resource($resource)) {
            return 0;
        }

        $id = (string)$resource;
        $id = substr($id, strpos($id, '#') + 1);
        return (int)$id;
    }

    /**
     * @return bool
     */
    public function request(): bool
    {
        if (empty($this->requests)) {
            return false;
        }

        $mh = curl_multi_init();

        foreach ($this->requests as $request) {
            curl_multi_add_handle($mh, $request['handle']);
        }

        $running = 0;
        do {
            curl_multi_exec($mh, $running);

            while (($request = curl_multi_info_read($mh)) !== false) {
                $callback = $this->requests[$this->getResourceId($request['handle'])]['callback'];

                if (is_callable($callback)) {
                    call_user_func(
                        $callback,
                        curl_multi_getcontent($request['handle']),
                        curl_getinfo($request['handle'])
                    );
                }

                curl_multi_remove_handle($mh, $request['handle']);
            }

            usleep(300);
        } while ($running > 0);

        curl_multi_close($mh);
        $this->requests = [];

        return true;
    }
}