<?php

namespace console\controllers;

use Codeception\Util\HttpCode;
use common\models\Cities;
use common\models\Forecast;
use console\components\MultiCURL;
use yii\console\ExitCode;
use yii\helpers\Console;
use DateTime;
use DOMDocument;
use DOMElement;
use DOMXPath;

/**
 * Class ForecastController
 * @package app\console\controllers
 */
class ForecastController extends \yii\console\Controller
{
    /**
     * @var string
     */
    private $url = 'http://quiz.dev.travelinsides.com/forecast/api/getForecast?';

    /**
     * @param string $start
     * @param string $end
     */
    public function actionIndex(string $start, string $end)
    {
        if (!$this->validateDate($start) || !$this->validateDate($end)) {
            Console::output($this->ansiFormat('Error: ', Console::FG_RED) . 'Invalid required arguments: Start, End');
            exit(ExitCode::DATAERR);
        }

        if (!$this->compareDates($start, $end)) {
            Console::output($this->ansiFormat('Error: ', Console::FG_RED) . 'Start cannot be greater than or equal End');
            exit(ExitCode::DATAERR);
        }

        $newForecastCount = 0;
        foreach (Cities::find()->batch(100) as $cities) {
            $multiCURL = new MultiCURL();
            foreach ($cities as $city) {
                /* @var Cities $city */

                $multiCURL->add(
                    $this->buildUrl($city->name, $start, $end),
                    function ($content, $info) use ($city, &$newForecastCount) {
                        if (isset($info['http_code']) && $info['http_code'] == HttpCode::OK) {
                            $dom = new DOMDocument();
                            if ($dom->loadXML($content)) {
                                $xpath = new DOMXPath($dom);
                                $rows = $xpath->query('//rows/row');
                                foreach ($rows as $row) {
                                    /* @var DOMElement $row */
                                    try {
                                        $forecast = new Forecast();
                                        $forecast->city_id = $city->id;
                                        $forecast->temperature = $row->getElementsByTagName('temperature')[0]->nodeValue;
                                        $forecast->when_created = $row->getElementsByTagName('ts')[0]->nodeValue;

                                        $forecast->save() ? ++$newForecastCount : false;
                                    } catch (\Exception $e) {
                                    }
                                }
                            }
                        }
                    }
                );
            }

            $multiCURL->request();
        }

        Console::output($this->ansiFormat("($newForecastCount) records were created!", Console::FG_GREEN));
        exit(ExitCode::OK);
    }

    /**
     * @param string $date
     * @return bool
     */
    private function validateDate(string $date): bool
    {
        $format = 'd.m.Y';
        $d = DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) === $date;
    }

    /**
     * @param string $start
     * @param string $end
     * @return bool
     */
    private function compareDates(string $start, string $end): bool
    {
        $format = 'd.m.Y';
        $start = DateTime::createFromFormat($format, $start);
        $end = DateTime::createFromFormat($format, $end);

        return $start < $end;
    }

    /**
     * @param string $city
     * @param string $start
     * @param string $end
     * @return string
     */
    private function buildUrl(string $city, string $start, string $end): string
    {
        return $this->url . http_build_query(compact('start', 'end', 'city'));
    }
}