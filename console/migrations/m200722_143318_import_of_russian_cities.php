<?php

use yii\db\Migration;
use yii\helpers\Json;

/**
 * Class m200722_143318_import_of_russian_cities
 */
class m200722_143318_import_of_russian_cities extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Города взяты с сайта: https://simplemaps.com/data/ru-cities
        $jsonCities = file_get_contents(__DIR__ . '/../data/ru.json');
        $cities = Json::decode($jsonCities);

        if (!empty($cities)) {

            // Insert country
            $country = $cities[0]['country'];
            $this->db->createCommand()->insert('{{%countries}}', ['name' => $country])->execute();
            $countryId = $this->db->getLastInsertID();

            $rows = [];
            foreach ($cities as $city) {
                $rows[] = [$city['city'], $countryId];
            }

            // Insert cities
            $this->batchInsert('{{%cities}}', ['name', 'country_id'], $rows);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('{{%countries}}');
        $this->truncateTable('{{%cities}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_143318_import_of_russian_cities cannot be reverted.\n";

        return false;
    }
    */
}
