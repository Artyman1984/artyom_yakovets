<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cities}}`.
 */
class m200721_180202_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->text()->notNull(),
        ]);

        $this->createIndex(
            'idx-cities-country_id',
            'cities',
            'country_id'
        );

        $this->addForeignKey(
            'fk-cities-country_id-countries-id',
            '{{%cities}}',
            'country_id',
            'countries',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-cities-country_id-countries-id',
            'cities'
        );

        $this->dropIndex(
            'idx-cities-country_id',
            'cities'
        );

        $this->dropTable('{{%cities}}');
    }
}
