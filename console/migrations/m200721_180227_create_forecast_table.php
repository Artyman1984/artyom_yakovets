<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%forecast}}`.
 */
class m200721_180227_create_forecast_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%forecast}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'temperature' => $this->double()->notNull(),
            'when_created' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-forecast-city_id',
            'forecast',
            'city_id'
        );

        $this->addForeignKey(
            'fk-forecast-city_id-cities-id',
            '{{%forecast}}',
            'city_id',
            'cities',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->db
            ->createCommand('
                CREATE UNIQUE INDEX 
                    "idx_unique-forecast-city_id-when_created" 
                ON 
                    "forecast" ("city_id", "when_created")')
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-unique-forecast-city_id-when_created',
            'forecast'
        );

        $this->dropForeignKey(
            'fk-forecast-city_id-cities-id',
            'forecast'
        );

        $this->dropIndex(
            'idx-forecast-city_id',
            'forecast'
        );

        $this->dropTable('{{%forecast}}');
    }
}