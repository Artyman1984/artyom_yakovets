<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cities}}".
 *
 * @property int $id
 * @property int $country_id
 * @property string $name
 *
 * @property Countries $country
 * @property Forecast[] $forecasts
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id'], 'default', 'value' => null],
            [['country_id'], 'integer'],
            [['name'], 'string'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::class, 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\CountriesQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::class, ['id' => 'country_id']);
    }

    /**
     * Gets query for [[Forecasts]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\ForecastQuery
     */
    public function getForecasts()
    {
        return $this->hasMany(Forecast::class, ['city_id' => 'id'])->orderBy('when_created DESC');
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\CitiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CitiesQuery(get_called_class());
    }
}
