<?php

namespace common\models;

use Yii;
use DateTime;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%forecast}}".
 *
 * @property int $id
 * @property int $city_id
 * @property float $temperature
 * @property int $when_created
 *
 * @property Cities $city
 */
class Forecast extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public $country_name;

    /**
     * @var string
     */
    public $city_name;

    /**
     * @var float
     */
    public $max_temperature;

    /**
     * @var float
     */
    public $min_temperature;

    /**
     * @var float
     */
    public $avg_temperature;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%forecast}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'temperature', 'when_created'], 'required'],
            [['city_id', 'when_created'], 'default', 'value' => null],
            [['city_id', 'when_created'], 'integer'],
            [['temperature'], 'number'],
            [['city_id', 'when_created'], 'unique', 'targetAttribute' => ['city_id', 'when_created']],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cities::class,
                'targetAttribute' => ['city_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'temperature' => 'Temperature',
            'when_created' => 'When Created',
            'country_name' => 'Country',
            'city_name' => 'City',
            'max_temperature' => 'Max Temperature',
            'min_temperature' => 'Min Temperature',
            'avg_temperature' => 'Avg Temperature',
        ];
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\CitiesQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\ForecastQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ForecastQuery(get_called_class());
    }

    /**
     * @param string $attrTemperature
     * @return string|null
     */
    public function fahrenheitToCelsius(string $attrTemperature): ?string
    {
        $availableAttributes = ['temperature', 'max_temperature', 'min_temperature', 'avg_temperature'];

        if (ArrayHelper::isIn($attrTemperature, $availableAttributes) && !is_null($this->$attrTemperature)) {
            $celsius = Yii::$app->formatter->asDecimal(($this->$attrTemperature - 32) * 5 / 9, 0);
            $sign = '';
            if ($celsius > 0) {
                $sign = '+';
            } elseif ($celsius < 0) {
                $sign = '-';
            }

            return $sign . abs($celsius) . ' &#8451;';
        }

        return null;
    }

    /**
     * @param Cities $city
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public static function getHistoryByCity(Cities $city): array
    {
        $history = [];
        foreach ($city->forecasts as $forecast) {
            $dateTime = (new DateTime())->setTimestamp($forecast->when_created);
            $history[Yii::$app->formatter->asDate($dateTime, 'long')][] = [
                'time' => $dateTime->format('H:i:s'),
                'temperature' => $forecast->fahrenheitToCelsius('temperature'),
            ];
        }

        return $history;
    }
}
