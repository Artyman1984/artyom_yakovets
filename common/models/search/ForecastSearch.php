<?php

namespace common\models\search;

use common\models\Forecast;
use yii\data\ActiveDataProvider;
use DateTime;

/**
 * Class ForecastSearch
 *
 * @property string $start
 * @property string $end
 * @package frontend\models\search
 */
class ForecastSearch extends Forecast
{
    /**
     * @var string
     */
    private $format = 'd.m.Y';

    /**
     * @var string
     */
    public $start;

    /**
     * @var string
     */
    public $end;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start', 'end'], 'date', 'format' => 'php:' . $this->format],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'start' => 'Start',
            'end' => 'End'
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = Forecast::find()
            ->alias('f')
            ->select(
                [
                    'country_name' => 'country.name',
                    'city_name' => 'city.name',
                    'max_temperature' => 'MAX(f.temperature)',
                    'min_temperature' => 'MIN(f.temperature)',
                    'avg_temperature' => 'AVG(f.temperature)',
                ]
            )
            ->innerJoin(['city' => 'cities'], 'city.id = f.city_id')
            ->innerJoin(['country' => 'countries'], 'country.id = city.country_id')
            ->groupBy('country.id, city.id');


        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => false,
            ]
        );

        $dataProvider->sort->attributes['country_name'] = [
            'asc' => ['country.name' => SORT_ASC],
            'desc' => ['country.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['city_name'] = [
            'asc' => ['city.name' => SORT_ASC],
            'desc' => ['city.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['max_temperature'] = [
            'asc' => ['max_temperature' => SORT_ASC],
            'desc' => ['max_temperature' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['min_temperature'] = [
            'asc' => ['min_temperature' => SORT_ASC],
            'desc' => ['min_temperature' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['avg_temperature'] = [
            'asc' => ['avg_temperature' => SORT_ASC],
            'desc' => ['avg_temperature' => SORT_DESC],
        ];

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if ($this->start && ($start = DateTime::createFromFormat($this->format, $this->start))) {
            $query->andWhere(['>=', 'when_created', $start->getTimestamp()]);
        }

        if ($this->end && ($end = DateTime::createFromFormat($this->format, $this->end))) {
            $query->andWhere(['<=', 'when_created', $end->getTimestamp()]);
        }

        return $dataProvider;
    }
}