<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%countries}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Cities[] $cities
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%countries}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Cities]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\CitiesQuery
     */
    public function getCities()
    {
        return $this->hasMany(Cities::class, ['country_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\CountriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CountriesQuery(get_called_class());
    }
}
