<?php

namespace frontend\controllers;

use Yii;
use common\models\Cities;
use common\models\Forecast;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\search\ForecastSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays statistics.
     *
     * @return mixed
     */
    public function actionStats()
    {
        $model = new ForecastSearch();
        $modelDataProvider = $model->search(Yii::$app->request->get());

        return $this->render('statistics', compact('model', 'modelDataProvider'));
    }

    /**
     * Displays history.
     *
     * @param string $city
     * @return string
     * @throws BadRequestHttpException
     * @throws Yii\base\InvalidConfigException
     */
    public function actionHistory(string $city)
    {
        $cityName = $city;
        $city = Cities::findOne(['name' => $cityName]);
        if (!$city) {
            throw new BadRequestHttpException("City '$cityName' was not found.");
        }

        $history = array_chunk(Forecast::getHistoryByCity($city), 4, true);

        return $this->render('history', compact('history', 'city'));
    }
}
