<?php

/* @var $this yii\web\View */

/* @var $model common\models\search\ForecastSearch */

/* @var $modelDataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use common\models\Forecast;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Statistics';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Search
                </div>
                <?php
                $form = ActiveForm::begin(
                    [
                        'id' => 'form-StatsForm',
                        'method' => 'GET',
                        'action' => ['/forecast/site/stats'],
                        'enableClientValidation' => false,
                        'fieldConfig' => [
                            'template' => "{label}<div class=\"input-group date dtp\" id='datepicker'>{input}<span class=\"input-group-addon\"><span class=\"glyphicon-calendar glyphicon\"></span></span></div>",
                        ],
                    ]
                ) ?>

                <div class="panel-body row">
                    <div class="col-xs-2">
                        <?= $form->field($model, 'start')->textInput(['readonly' => 'readonly']) ?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($model, 'end')->textInput(['readonly' => 'readonly']) ?>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" style="margin-top: 25px"><span
                                        class="glyphicon glyphicon-search"></span> Search
                            </button>
                        </div>
                    </div>
                </div>

                <?php
                ActiveForm::end(); ?>

            </div>
        </div>
        <?php
        Pjax::begin(
            [
                'id' => 'forecast-pjax',
                'timeout' => 3000,
            ]
        ) ?>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">

                    <?= GridView::widget(
                        [
                            'dataProvider' => $modelDataProvider,
                            'tableOptions' => ['class' => 'table table-bordered'],
                            'showOnEmpty' => false,
                            'columns' => [
                                [
                                    'attribute' => 'country_name',
                                ],
                                [
                                    'attribute' => 'city_name',
                                ],
                                [
                                    'attribute' => 'max_temperature',
                                    'format' => 'html',
                                    'value' => function (Forecast $model) {
                                        return $model->fahrenheitToCelsius('max_temperature');
                                    }
                                ],
                                [
                                    'attribute' => 'min_temperature',
                                    'format' => 'html',
                                    'value' => function (Forecast $model) {
                                        return $model->fahrenheitToCelsius('min_temperature');
                                    }
                                ],
                                [
                                    'attribute' => 'avg_temperature',
                                    'format' => 'html',
                                    'value' => function (Forecast $model) {
                                        return $model->fahrenheitToCelsius('avg_temperature');
                                    }
                                ],
                                [
                                    'header' => 'Actions',
                                    'value' => function (Forecast $model) {
                                        return '';
                                    }
                                ],
                            ],
                        ]
                    ) ?>

                </div>
            </div>
        </div>

        <?php
        Pjax::end(); ?>

    </div>
</div>

<?php
$this->registerJs(
    <<<JS
    $(function () {
        $('.dtp:first').datetimepicker({
          ignoreReadonly: true,
          format: 'DD.MM.YYYY'          
        });
        $('.dtp:last').datetimepicker({
          ignoreReadonly: true,
          format: 'DD.MM.YYYY',          
          useCurrent: false
        });
        $(".dtp:first").on("dp.change", function (e) {
            $('.dtp:last').data("DateTimePicker").minDate(e.date);
        });
        $(".dtp:last").on("dp.change", function (e) {
            $('.dtp:first').data("DateTimePicker").maxDate(e.date);
        });
    });
    
    $(document).on("submit", "[id='form-StatsForm']", function(event) {
        $.pjax.reload(
            "#forecast-pjax", 
            {
                "url": "/forecast/site/stats", 
                "type": "GET",
                "data": {
                    "{$model->formName()}[start]": $("[name='{$model->formName()}[start]']").val(),
                    "{$model->formName()}[end]": $("[name='{$model->formName()}[end]']").val(),
                }
            }
        );
        return false;
    });
JS
);
?>
