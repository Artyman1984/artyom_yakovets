<?php
/* @var $this yii\web\View */
/* @var $city common\models\Cities */
/* @var $history array */

use yii\helpers\Html;
?>

<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    History of <?= Html::encode($city->name)?> (<?= Html::encode($city->country->name ?? '')?>)
                </div>
                <div class="panel-body">
                    <?php foreach ($history as $dayHistory): ?>
                        <div class="row">
                            <?php foreach ($dayHistory as $date => $times): ?>
                                <div class="col-xs-3">
                                    <p><strong><?= Html::encode($date)?></strong></p>
                                    <?php foreach ($times as $time): ?>
                                        <p><?= $time['time'] . ' ' . $time['temperature']?></p>
                                    <?php endforeach; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
